<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'mariadb-wordpress' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'By))_>e SY4Ik9U/XQHi6tYkU(mPcOYcioM10/.X:MaUyt#mFc{[O&?Nr.yC%+k=' );
define( 'SECURE_AUTH_KEY',  ',COTxJ^5HJ=cLH|DS}evLPT-2EkK%a@8.;=S^tc K^,5Qu$Sa|XR$tCW:h5T(Mtk' );
define( 'LOGGED_IN_KEY',    'jK%NcD!$ckaL@W%2h]i![*D*~L1b.oMl_R{>UgFU[_iPdQ,%@[]nx.Cuv(*)5(y5' );
define( 'NONCE_KEY',        'iGxI6k={Y,`%tE,Y#EHhMiU,A}a5K}A TDJ?0&B.8!KJ8*9AnZ=)uL47.lPQ> ?V' );
define( 'AUTH_SALT',        'ZnzRoIH%zjR(PK;pjkevQHmlE+gUuxV^^g<!3tLHn7a/cmYP-BCK&dFbwQ-C3-Ot' );
define( 'SECURE_AUTH_SALT', '8f#T6Lwd A]Z2cUALAvWd2U]e/uAog}&g$a:?$_U[-x*9MbEf-z=37(N;j3z1;@4' );
define( 'LOGGED_IN_SALT',   'fEEjLu.O(vg:?DV]PRMP[KX8*=5KUw<;u:?mH1jLH{^EW`RdiwxZ=Zcj9_5ONGZ(' );
define( 'NONCE_SALT',       'gV}Z9^6Jk9$ pz}`>VaKIdVeQ`A&ixJ/5qSeoux3a}}5u6~c%x3y%>fmKmuphCT%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

define('FS_METHOD','direct');
